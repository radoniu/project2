﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public GameObject medAsteroid;
    public GameObject smlAsteroid;

    private GameObject playerShip;
    private Vector2 playerPOS;

    public float enemySpeed;
    public float enemyTurn;
    public float topBorder;
    public float bottomBorder;
    public float rightBorder;
    public float leftBorder;

    private GameController gameController;

	// Use this for initialization
	void Start () {

        // Get a reference to the game controller object and the script
        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameController>();

        playerShip = GameObject.FindGameObjectWithTag("Player");
        playerPOS = new Vector3(playerShip.transform.position.x, playerShip.transform.position.y, playerShip.transform.position.z);
        Vector3 check = new Vector3(0, 0, 0);

        if (playerPOS.Equals(check))
        {
            playerPOS = new Vector3(1, 1, 0);
        }

        GetComponent<Rigidbody2D>().AddForce(playerPOS * enemySpeed);
        GetComponent<Rigidbody2D>().AddTorque(Random.Range(-enemyTurn, enemyTurn));


    }
    void Update()
    {
        if(GetComponent<Transform>().position.x < leftBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
        if (GetComponent<Transform>().position.x > rightBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
        if (GetComponent<Transform>().position.y < bottomBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
        if (GetComponent<Transform>().position.y > topBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
    }

    void OnCollisionEnter2D(Collision2D colision)
    {

        if (colision.gameObject.tag.Equals("Bullet"))
        {

            // Destroy the bullet
            Destroy(colision.gameObject);

            // If large asteroid spawn new ones
            if (tag.Equals("Large Asteroid"))
            {
                // Spawn Med Asteroid
                Instantiate(medAsteroid,
                    new Vector3(transform.position.x,
                        transform.position.y, 0),
                        Quaternion.Euler(0, 0, 90));

                // Spawn small asteroids
                Instantiate(smlAsteroid,
                    new Vector3(transform.position.x,
                        transform.position.y, 0),
                        Quaternion.Euler(0, 0, 0));

                gameController.SplitAsteroid(); // +2

            }
            else if (tag.Equals("Med Asteroid"))
            {
                // Spawn small asteroids
                Instantiate(smlAsteroid,
                    new Vector3(transform.position.x,
                        transform.position.y, 0),
                        Quaternion.Euler(0, 0, 0));

                // Spawn small asteroids
                Instantiate(smlAsteroid,
                    new Vector3(transform.position.x,
                        transform.position.y, 0),
                        Quaternion.Euler(0, 0, 90));


                gameController.SplitAsteroid(); // +2
            }
            else
            {
                // Just a small asteroid destroyed
                gameController.DecrementAsteroids();
            }

           
            // Add to the score
            gameController.IncrementScore();

            // Destroy the current asteroid
            Destroy(gameObject);

        }

    }
   
}

