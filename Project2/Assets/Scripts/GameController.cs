﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject asteroids;
    public GameObject enemyShip;
    public GameObject player;


    private int score;
    private int lives;
    private int highscore;
    private int asteroidsRemaining;

    public float topBorder;
    public float bottomBorder;
    public float rightBorder;
    public float leftBorder;

    public float rotationSpeed;

    public Text scoreText;
    public Text livesText;
    public Text highscoreText;




    // Use this for initialization
    void Start () {
        highscore = PlayerPrefs.GetInt("highscore", 0);
        BeginGame();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("escape"))
            Application.Quit();
      
    }

    void BeginGame()
    {
        score = 0;
        lives = 3;

        scoreText.text = "SCORE:" + score;
        highscoreText.text = "HISCORE: " + highscore;
        livesText.text = "LIVES: " + lives;

        SpawnAsteroids();
    }

    void SpawnAsteroids()
    {
        DestroyExistingAsteroids();

        asteroidsRemaining = 3;
        bool ship = false;

        for (int i = 0; i < asteroidsRemaining; i++)
        {
            if (Random.Range(0, 5) == 1 && ship == false)
            {
                //Spawm enemyship
                Instantiate(enemyShip, new Vector3(Random.Range(leftBorder, rightBorder),
                    Random.Range(bottomBorder, topBorder), 0), Quaternion.Euler(0, 0, 0));
                ship = true;
            }
            else
            {
                // Spawn an asteroid
                Instantiate(asteroids, new Vector3(Random.Range(leftBorder, rightBorder),
                        Random.Range(bottomBorder, topBorder), 0),
                    Quaternion.Euler(0, 0, Random.Range(-rotationSpeed, rotationSpeed)));
            }
        }
    }
    public void DecrementLives()
    {
        lives--;
        livesText.text = "LIVES: " + lives;

        SpawnAsteroids();

        // Has player run out of lives?
        if (lives < 1)
        {
            Application.Quit();
        }
    }
    void DestroyExistingAsteroids()
    {
        GameObject[] asteroids =
            GameObject.FindGameObjectsWithTag("Large Asteroid");

        foreach (GameObject current in asteroids)
        {
            GameObject.Destroy(current);
        }

        GameObject[] asteroids2 =
            GameObject.FindGameObjectsWithTag("Small Asteroid");

        foreach (GameObject current in asteroids2)
        {
            GameObject.Destroy(current);
        }

        GameObject[] asteroids3 =
           GameObject.FindGameObjectsWithTag("Med Asteroid");

        foreach (GameObject current in asteroids3)
        {
            GameObject.Destroy(current);
        }

        GameObject[] ships =
          GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject current in ships)
        {
            GameObject.Destroy(current);
        }
    }
    public void SplitAsteroid()
    {
        asteroidsRemaining += 1;
    }

    public void DecrementAsteroids()
    {
        asteroidsRemaining--;
    }
    public void IncrementScore()
    {
        score++;
        scoreText.text = "SCORE:" + score;

        if (score > highscore)
        {
            highscore = score;
            highscoreText.text = "HISCORE: " + highscore;

            // Save the new hiscore
            PlayerPrefs.SetInt("hiscore", highscore);
        }

        // Has player destroyed all asteroids?
        if (asteroidsRemaining == 0)
        {

           SpawnAsteroids();

        }
    }
}
