﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    private GameController gameController;
    private Vector2 playerPos;
    private GameObject playerShip;


    public float force;

    // Use this for initialization
    void Start()
    {
        GameObject gameControllerObject =
           GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameController>();

        playerShip = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        playerPos = new Vector3(playerShip.transform.position.x, playerShip.transform.position.y, playerShip.transform.position.z);
        GetComponent<Rigidbody2D>().position = Vector3.MoveTowards(transform.position, playerPos, force * Time.deltaTime);


        if (GetComponent<Transform>().position.x < gameController.leftBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
        if (GetComponent<Transform>().position.x > gameController.rightBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
        if (GetComponent<Transform>().position.y < gameController.bottomBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }
        if (GetComponent<Transform>().position.y > gameController.topBorder)
        {
            Destroy(gameObject);
            gameController.DecrementAsteroids();
        }

    }
    void OnCollisionEnter2D(Collision2D colider)
    {
        if (colider.gameObject.tag.Equals("Bullet"))
        {
            // Destroy the bullet
            Destroy(colider.gameObject);
            // remove 1
            gameController.DecrementAsteroids();
            // add to score
            gameController.IncrementScore();
            // destroy object
            Destroy(gameObject);
        }


    }
}
