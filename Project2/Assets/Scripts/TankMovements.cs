﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovements : MonoBehaviour {

    public float moveSpeed;
    public float turnSpeed;
    public float bulletSpeed;
    public float bulletLife;
    public float topBorder;
    public float bottomBorder;
    public float rightBorder;
    public float leftBorder;

    private Transform tf;
    public GameObject bullet;
    private GameController gameController;

    // Use this for initialization
    void Start()
    {
        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameController>();

        tf = GetComponent<Transform>();
        tf.position =  new Vector3(0f, 0f, 0f);

    }

    // Update is called once per frame
    void Update()
    {
        //Fire
        if (Input.GetKeyDown(KeyCode.Space))
        {
           GameObject newbullet = Instantiate(bullet, transform.position, transform.rotation);
            newbullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * bulletSpeed);
            Destroy(newbullet, bulletLife);
        }

       //Movement
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            tf.Rotate(0, 0, turnSpeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            tf.Rotate(0, 0, -turnSpeed);

        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            tf.Translate(Vector2.up * moveSpeed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            tf.Translate(Vector2.down * moveSpeed);
        }

        if (tf.position.x < leftBorder)
        {
            tf.position = new Vector3(0f, 0f, 0f);
            gameController.DecrementLives();
        }
        if (tf.position.x > rightBorder)
        {
            tf.position = new Vector3(0f, 0f, 0f);
            gameController.DecrementLives();
        }
        if (tf.position.y > topBorder)
        {
            tf.position = new Vector3(0f, 0f, 0f);
            gameController.DecrementLives();
        }
        if (tf.position.y < bottomBorder)
        {
            tf.position = new Vector3(0f, 0f, 0f);
            gameController.DecrementLives();
        }

    }
    void OnTriggerEnter2D(Collider2D collider)
    {

        // Anything except a bullet is an asteroid
        if (collider.gameObject.tag != "Bullet")
        {

            // Move the ship to the centre of the screen
            tf.position = new Vector3(0, 0, 0);

            gameController.DecrementLives();

        }
    }
}


